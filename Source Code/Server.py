import multiprocessing as mp
import socket
import AgentHandler
import sqlite3

HOST = socket.gethostbyname('localhost')
PORT = 50000
serverSock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
serverSock.bind((HOST,PORT))
#processPool = mp.Pool(processes=4)


def main():
    lock = mp.Lock()
    while True:
        serverSock.listen(5)
        print('waiting for connection...')
        clientConn, clientAddr = serverSock.accept()
        print("connected to client "+str(clientAddr)+" "+str(clientConn))
        dbConn = sqlite3.connect('PubSub.db')
        process = mp.Process(target=AgentHandler.handleAgent, args=(clientConn,clientAddr,dbConn,lock))
        process.daemon = True
        process.start()


if __name__ == '__main__':
    main()