import json
import time
import traceback
import sys
import pickle

clientUserName = None

def main():
    pass


def handleAgent(clientConn,clientAddr,dbConn,lock):
    try:
        print("handling a client...."+str(clientAddr))
        cursor = dbConn.cursor()
        handleRegister(clientConn,clientAddr,cursor,lock,dbConn)
        optionsData = pickle.dumps(getOptions())
        clientConn.sendall(optionsData)
        while True:
            choice = int(clientConn.recv(1024).decode('utf-8'))
            if choice == 1:
                handleShowTopics(clientConn,cursor,lock)
            elif choice == 8:
                break
            elif choice == 2:
                handleSubscribe(clientConn,dbConn,cursor,lock)
        sys.exit(1)
    except Exception as e:
        print(str(e)+ " exception in handleAgent ")
        clientConn.close()
        traceback.print_exc()
        sys.exit(-1)


def handleRegister(clientConn,clientAddr,cursor,lock,dbConn):
    userdata = clientConn.recv(1024).decode('utf-8').split(',')
    uname = userdata[0]
    port = userdata[1]
    lock.acquire()
    cursor.execute("SELECT * FROM user where username = ?",(uname,))
    data = cursor.fetchone()
    if data == None:
        clientConn.sendall(bytes("Registerd as a new client ", 'utf-8'))
        insTuple = (uname, clientAddr[0], str(port))
        cursor.execute("INSERT INTO user VALUES(?,?,?)", insTuple)
        global clientUserName
        clientUserName = uname
    elif data != None and data[1] == clientAddr[0]:
        clientConn.sendall(bytes("welcome "+str(uname),'utf-8'))
        clientUserName = uname
    elif data != None and data[1] != clientAddr[0]:
        clientConn.sendall(bytes("username already in use",'utf-8'))
    dbConn.commit()
    lock.release()


def handleShowTopics(clientSock,cursor,lock):
    lock.acquire()
    cursor.execute("SELECT * FROM topic")
    data = cursor.fetchall()
    lock.release()
    topicStr = ""
    for row in data:
        topicStr += str(row[0])+"-"+str(row[1])+"\n"
    clientSock.sendall(bytes(topicStr,'utf-8'))


def handleSubscribe(clientConn,dbConn,curosr,lock):
    clientConn.sendall(bytes("enter the topic id you want to subscribe",'utf-8'))
    choice = clientConn.recv(1024).decode("utf-8")
    lock.acquire()
    global clientUserName
    inpTuple = (clientUserName,int(choice))
    curosr.execute("INSERT INTO subscriber VALUES(?,?)",inpTuple)
    dbConn.commit()
    lock.release()
    clientConn.sendall(bytes("successfully added",'utf-8'))


def getOptions():
    options = {1:'show all topics',2:'Subscribe to a topic',3:'Add a new topic',8:'exit'}
    return options
