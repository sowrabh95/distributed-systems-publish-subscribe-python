import socket
import json
import sys
import pickle

HOST = socket.gethostbyname('localhost')
PORT_TO_CONNECT = 50000
PORT_TO_RECIVE = None
username = None

def main():
    register()
    clientSock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    clientSock.connect((HOST,PORT_TO_CONNECT))
    print(" connected ")
    if getRegistered(clientSock) == False:
        clientSock.close()
        sys.exit(-1)
    optionsData = clientSock.recv(1024)
    options = pickle.loads(optionsData)
    methodMap = getTaskMap()
    while True:
        printOptions(options)
        print(options.keys())
        inputOption = input("Enter the choice")
        if inputOption.isnumeric() == False:
            print("enter proper option")
            continue
        elif int(inputOption) not in options.keys():
            print("enter an option within the given choices")
            continue
        else:
            methodMap[int(inputOption)](clientSock,inputOption)


def register():
    uname = input("enter username")
    port = input("enter port to get notifications")
    global username
    username = uname
    global PORT_TO_RECIVE
    PORT_TO_RECIVE = port



def getRegistered(clientSock):
    clientSock.sendall(bytes(str(username)+","+str(PORT_TO_RECIVE),'utf-8'))
    regisData = clientSock.recv(1024).decode('utf-8')
    print(regisData)
    if regisData == "username already in use":
        return False
    else:
        return True


def handleShowTopics(clientSock,inputOption):
    clientSock.sendall(bytes(str(inputOption),'utf-8'))
    topicStr = clientSock.recv(1024).decode('utf-8')
    print(topicStr)


def handleSubcribeTopic(clientSock,inputOption):
    clientSock.sendall(bytes(str(inputOption),'utf-8'))
    question = clientSock.recv(1024).decode('utf-8')
    choice = input(question)
    clientSock.sendall(bytes(choice,'utf-8'))
    reply = clientSock.recv(1024).decode("utf-8")
    print(reply)


def handleAddTopic():
    pass


def printOptions(options):
    for key in options:
        print(str(key)+"-"+options[key])


def getTaskMap():
    methodMap = {1:handleShowTopics,2:handleSubcribeTopic,3:handleAddTopic}
    return methodMap


if __name__ == '__main__':
    main()