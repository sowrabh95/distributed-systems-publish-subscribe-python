import sqlite3

def main():
    dbConn = sqlite3.connect('PubSub.db')
    cursor = dbConn.cursor()
    createTables(cursor)
    populateTabes(cursor)
    dbConn.commit()

def createTables(cursor):
    cursor.execute("CREATE TABLE topic(id INTEGER PRIMARY KEY,name TEXT)")
    cursor.execute("CREATE TABLE user(username TEXT PRIMARY KEY,ip_address TEXT,port INTEGER)")
    cursor.execute("CREATE TABLE notification(username TEXT,content TEXT, CONSTRAINT fk_username "
                   "FOREIGN KEY(username) REFERENCES user(username))")
    cursor.execute("CREATE TABLE subscriber(username TEXT,topic INTEGER, CONSTRAINT fk_sub_username "
                   "FOREIGN KEY(username) REFERENCES user(username), CONSTRAINT fk_sub_topic "
                   "FOREIGN KEY(topic) REFERENCES topic(id))")



def populateTabes(cursor):
    cursor.execute("INSERT INTO topic VALUES(1,'politics')")
    cursor.execute("INSERT INTO topic VALUES(2,'sports news')")





if __name__ == '__main__':
    main()